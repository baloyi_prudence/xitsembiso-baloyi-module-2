
void main () {
  //Creating an array to store winning apps 
   var listOfWinningApps = ["FNB-2012","SnapScan-2013","LIVE inspect-2014","WumDrop-2015","Domestly-2016",
                          "Shyft-2017","Khula ecosystem-2018","Naked Insurance-2019","EasyEquities-2020","Ambani Africa-2021"];
 
  //sort the array by names
  listOfWinningApps.sort();
  
  //output the list of the winning apps of the MTN Business App of the Year Award since 2012
  print ("The list of winning apps of the MTN Business App of the Year Awards from 2012 is \n$listOfWinningApps\n");  
  
  //output the winning app of 2017 and 2018
  print ("Winning app of 2017 is ${listOfWinningApps[7]},and winning app of 2018 is ${listOfWinningApps[4]}\n");
  
  //output the total number of winning apps from 2012 until 2021
  print ("The total number of winning apps is ${listOfWinningApps.length}");
}
